#### Money transfer between accounts
1. For API take a look to `IntegrationTest` or `ApiTest` or `api.yaml`.
2. To build run `build.sh`.
3. Requires:
  * sbt in version 1.0 or higher
  * docker
4. To run docker image
  * `./build.sh`
  * `docker run -d -p 8080:8080 tmaslanka/money-transfer:1.0-SNAPSHOT`
