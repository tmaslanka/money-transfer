package tmaslanka.money.transfer

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

trait FuturesSupport {
  implicit class FutureOps[T](f: Future[T]) {
    def await: T = Await.result(f, 1.second)
  }
}
