package tmaslanka.money.transfer.model

import tmaslanka.money.transfer.model.domain.{AccountNumber, ExampleDomainObjects, Money, TransactionId}
import tmaslanka.money.transfer.model.messages._


object ExampleCommands {
  import ExampleDomainObjects._

  val createAccountCommand = CreateAccountCommand(PLN, userId)
  val createAccountCommandResponse = CreateAccountCommandResponse(accountNumber)

  val depositDto = DepositDTO(TransactionId.generate, 10.0, userId, "some deposit")
  val moneyTransferDto = MoneyTransferDTO(TransactionId.generate, Some(accountNumber), secondAccountNumber, 8.0, userId, "some transfer")

  val makeDepositCommand = MakeAccountTransactionCommand(depositDto)

  val makeMoneyTransfer = MakeAccountTransactionCommand(moneyTransferDto)

  def transferTo(toAccount: AccountNumber, amount: Money): MakeAccountTransactionCommand = {
    makeMoneyTransfer.copy(
      transaction = moneyTransferDto.copy(id = TransactionId.generate, toAccount = toAccount, amount = amount))
  }
}
