package tmaslanka.money.transfer.model.domain

import cats.data.Writer
import tmaslanka.money.transfer.model.messages.TransactionError

object AccountReducerSupport {
  def reduce(account: Account)(events: TransactionEvent*): (Account, Option[TransactionError]) = {

    def reduceState(accountW: Writer[Vector[TransactionError], Account], event: TransactionEvent): Writer[Vector[TransactionError], Account] = {
      import cats.implicits._
      accountW.flatMap { account =>
        val (s1, errors) = AccountReducer.reduceAccount(account, event)
        Writer(errors.toVector, s1)
      }
    }

    val value = Writer.apply(Vector.empty[TransactionError], account)
    val (errors, s1) = events.foldLeft(value)(reduceState).run
    s1 -> errors.headOption
  }
}
