package tmaslanka.money.transfer.model.domain

import org.scalacheck.Gen
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{FlatSpec, MustMatchers}

import scala.collection.mutable.ArrayBuffer
import scala.util.Random

class AccountReducerPropsTest extends FlatSpec with GeneratorDrivenPropertyChecks with MustMatchers {
  import ExampleDomainObjects._

  val precision = 2
  val intCents = 100

  val amountGen: Gen[Money] = for {
    amountCents <- Gen.chooseNum[Int](-1000, 1000)
  } yield BigDecimal(amountCents, 2)

  val transactionsGen: Gen[List[Transaction]] = for {
    size <- Gen.choose(1, 100)
    amounts <- Gen.listOfN(size, amountGen)
    rand <- Gen.choose(0, 1)
    transactions <- amounts.filter(_ != 0).map { amount =>
      if (amount < 0)
        outgoingTransfer.copy(id = TransactionId.generate, amount = amount)
      else if (rand == 0)
        incomingTransfer.copy(id = TransactionId.generate, amount = amount)
      else
        deposit.copy(id = TransactionId.generate, amount = amount)
    }
  } yield transactions


  "all operations " should "preserve consistent state" in {
    forAll(transactionsGen) { transactions =>
      val events = createTransactionEvents(transactions)
      println(events)
      val (account, error) = AccountReducerSupport.reduce(emptyAccount)(events:_*)

      (account.balance * intCents) mustBe 'validInt
      (account.balance * intCents).toInt must be >= 0

      val moneys = events.zipWithIndex.collect {
        case (TransactionStarted(tr), idx) if tr.amount < 0 =>
          Some(tr.amount)
        case (TransactionCommitted(id), idx) =>
          events.collectFirst { case TransactionStarted(tr) if tr.id == id && tr.amount > 0 => tr.amount }
      }.collect{case Some(v) => v}

      println(s"moneys $moneys")

      val expectedBalance = moneys.foldLeft[Money](0.00) { (balance, eventAmount) =>
        if (balance + eventAmount >= 0) {
          println(s"adding $eventAmount")
          balance + eventAmount
        } else balance
      }

      println(s"account.balance: ${account.balance} expected: $expectedBalance")
      account.balance mustEqual expectedBalance
    }
  }

  private def createTransactionEvents(transactions: List[Transaction]) = {
    val starts = transactions.toVector.map(TransactionStarted)
    val commits = transactions.toVector.map(_.id).map(TransactionCommitted)

    val events = ArrayBuffer[TransactionEvent]()
    var startsIdx = 0
    var commitsIdx = 0
    while (startsIdx < starts.size) {
      while (startsIdx > commitsIdx && Random.nextBoolean()) {
        events += commits(commitsIdx)
        commitsIdx += 1
      }
      events += starts(startsIdx)
      startsIdx += 1
    }

    events ++= commits.drop(commitsIdx)
    assert(events.size == 2 * transactions.size)
    assert(events.collect{case s@TransactionStarted(_) => s}.size == transactions.size)

    val result = events.toList
    val values = result.groupBy(_.transactionId).values
    every(values) must have size 2
    result
  }
}
