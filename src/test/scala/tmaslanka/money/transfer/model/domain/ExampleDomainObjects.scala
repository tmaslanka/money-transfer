package tmaslanka.money.transfer.model.domain

import java.time.Instant

object ExampleDomainObjects  {
  val PLN = Currency("PLN")
  val userId = UserId("123")

  val accountNumber = AccountNumber.generate
  val secondAccountNumber = AccountNumber.generate
  val unknownAccount = AccountNumber("unknown")

  val emptyAccount = Account(accountNumber, userId, PLN)
  val account = Account(accountNumber, userId, PLN, 10.45)

  val deposit = Deposit(TransactionId.generate, amount = 1.5, userId = userId, description = "some deposit", dateTime = Instant.now())
  val withdrawal = Withdrawal(TransactionId.generate, amount = -1.5, description = "some withdrawal", userId = userId, dateTime = Instant.now())

  val incomingTransfer = IncomingTransfer(TransactionId.generate, fromAccount = accountNumber, amount = 10.25,
    description = "", userId = userId, dateTime = Instant.now())

  val outgoingTransfer = OutgoingTransfer(TransactionId.generate, toAccount = accountNumber, amount = -10.25,
    description = "", userId = userId, dateTime = Instant.now())


  implicit class AccountOps(account: Account) {
    def withPending(tr: Transaction): Account = {
      account.copy(transactions = Transactions(pending = Map(tr.id -> tr)))
    }

    def withAcceptedTransaction(tr: Transaction): Account = {
      account
        .modifyBalance(_ => tr.amount)
        .modifyTransactions(_.withTransaction(tr.withState(Accepted)))
    }
  }
}
