package tmaslanka.money.transfer.model.domain

import org.scalatest.{FlatSpec, MustMatchers}
import tmaslanka.money.transfer.model.domain.AccountReducer.reduceAccount
import tmaslanka.money.transfer.model.messages.BalanceExceeded

class AccountReducerTest extends FlatSpec with MustMatchers {
  import ExampleDomainObjects._

  val depositMoney: Money = 10

  val deposit = ExampleDomainObjects.deposit.copy(amount = depositMoney)
  val secondDeposit = deposit.copy(id = TransactionId.generate, amount = 1.14)

  val outTransfer = ExampleDomainObjects.outgoingTransfer.copy(amount = -depositMoney)
  val secondOutTransfer = outTransfer.copy(id = TransactionId.generate)

  val inTransfer = ExampleDomainObjects.incomingTransfer.copy(amount = depositMoney)

  "start commit deposit" should "add amount" in {
    val (s1, error1) = reduceAccount(emptyAccount, TransactionStarted(deposit))

    error1 mustBe None

    s1.balance mustEqual 0
    s1.transactions.accepted must have size 0
    s1.transactions.pending must have size 1
    s1.transactions.pending.head mustEqual deposit.id -> deposit.withState(Pending)

    val (s2, error2) = reduceAccount(s1, TransactionCommitted(deposit.id))

    error2 mustBe None

    s2.balance mustEqual deposit.amount
    s2.transactions.accepted must have size 1
    s2.transactions.pending must have size 0
    s2.transactions.accepted(0) mustEqual deposit.withState(Accepted)
  }

  "two deposits" should "be added to balance" in {

    val (s1, error) = AccountReducerSupport.reduce(emptyAccount)(
      TransactionStarted(deposit),
      TransactionCommitted(deposit.id),
      TransactionStarted(secondDeposit),
      TransactionCommitted(secondDeposit.id)
    )

    error mustBe None

    s1.balance mustEqual depositMoney + 1.14
    s1.blocked mustEqual 0
    s1.transactions.accepted must have size 2
    s1.transactions.accepted(0) mustEqual secondDeposit.withState(Accepted)
    s1.transactions.accepted(1) mustEqual deposit.withState(Accepted)
  }

  "start commit outgoing" should "remove amount and accept transaction" in {
    val (s0, error0) = makeDeposit(emptyAccount, deposit)

    error0 mustBe None

    s0.balance mustEqual depositMoney

    val (s1, error1) = reduceAccount(s0, TransactionStarted(outTransfer))

    error1 mustBe None

    s1.balance mustEqual 0
    s1.blocked mustEqual depositMoney
    s1.transactions.accepted must have size 1
    s1.transactions.pending must have size 1
    s1.transactions.pending.head mustEqual outTransfer.id -> outTransfer.withState(Pending)

    val (s2, error2) = reduceAccount(s1, TransactionCommitted(outTransfer.id))

    error2 mustBe None

    s2.balance mustEqual 0
    s2.blocked mustEqual 0
    s2.transactions.accepted must have size 2
    s2.transactions.pending must have size 0
    s2.transactions.accepted(0) mustEqual outTransfer.withState(Accepted)
  }

  "start start commit commit outgoing" should "remove amount and accept transactions" in {
    val (s0, error0) = makeDeposit(emptyAccount, deposit.copy(amount = 2 * depositMoney))

    error0 mustBe None

    val (s1, error1) = reduceAccount(s0, TransactionStarted(outTransfer))

    error1 mustBe None

    s1.balance mustEqual depositMoney
    s1.blocked mustEqual depositMoney

    val (s2, error2) = reduceAccount(s1, TransactionStarted(secondOutTransfer))

    error2 mustBe None

    s2.balance mustEqual 0
    s2.blocked mustEqual 2 * depositMoney

    val (s3, error3) = reduceAccount(s2, TransactionCommitted(outTransfer.id))

    error3 mustBe None

    s3.balance mustEqual 0
    s3.blocked mustEqual depositMoney
    s3.transactions.accepted must have size 2
    s3.transactions.pending must have size 1
    s3.transactions.accepted(0) mustEqual outTransfer.withState(Accepted)

    val (s4, error4) = reduceAccount(s3, TransactionCommitted(secondOutTransfer.id))

    error4 mustBe None

    s4.balance mustEqual 0
    s4.blocked mustEqual 0
    s4.transactions.accepted must have size 3
    s4.transactions.pending must have size 0
    s4.transactions.accepted(0) mustEqual secondOutTransfer.withState(Accepted)
  }

  "start commit commit outgoing" should "be idempotent" in {
    val (s0, error0) = makeDeposit(emptyAccount, deposit.copy(amount = 2 * depositMoney))

    error0 mustBe None

    val (s1, error1) = reduceAccount(s0, TransactionStarted(outTransfer))

    error1 mustBe None

    s1.balance mustEqual depositMoney
    s1.blocked mustEqual depositMoney

    val (s2, error2) = reduceAccount(s1, TransactionCommitted(outTransfer.id))

    error2 mustBe None

    val (s3, error3) = reduceAccount(s2, TransactionCommitted(outTransfer.id))

    error3 mustBe None
    s3 mustEqual s2
  }

  "outgoing start" should "reject transaction if balance exceeded" in {
    assert(depositMoney - 2 > 0, "make sure we deposit positive amount")

    val (s0, error0) = makeDeposit(emptyAccount, deposit.copy(amount = depositMoney - 2))

    error0 mustBe None

    s0.transactions.accepted must have size 1

    val (s1, error1) = reduceAccount(s0, TransactionStarted(outTransfer))

    error1 mustBe Some(BalanceExceeded)

    s1 mustEqual s0.withTransactionAdded(outTransfer.withState(Rejected))

    val (s2, _) = reduceAccount(s1, TransactionCommitted(outTransfer.id))

    s2 mustEqual s1
  }

  "rejected outgoing transaction" should "allow second out transaction with proper amount" in {
    val (s0, error0) = makeDeposit(emptyAccount, deposit.copy(amount = 7))

    error0 mustBe None

    s0.transactions.accepted must have size 1

    val (s1, error1) = reduceAccount(s0, TransactionStarted(outTransfer.copy(amount = -10)))

    error1 mustBe Some(BalanceExceeded)

    val secondTransfer = outTransfer.copy(id = TransactionId.generate, amount = -5)

    val (s2, error2) = reduceAccount(s1, TransactionStarted(secondTransfer))

    error2 mustBe None

    s2.blocked mustEqual 5
    s2.transactions.pending must have size 1
    s2.transactions.pending(secondTransfer.id) mustEqual secondTransfer.withState(Pending)
  }

  "incoming start commit" should "increase balance" in {
    val (s1, error1) = reduceAccount(emptyAccount, TransactionStarted(incomingTransfer))

    error1 mustBe None

    s1.balance mustEqual 0
    s1.blocked mustEqual 0
    s1.transactions.accepted must have size 0
    s1.transactions.pending must have size 1
    s1.transactions.pending(incomingTransfer.id) mustEqual incomingTransfer.withState(Pending)

    val (s2, error2) = reduceAccount(s1, TransactionCommitted(incomingTransfer.id))

    error2 mustBe None

    s2.balance mustEqual incomingTransfer.amount
    s2.blocked mustEqual 0
    s2.transactions.accepted must have size 1
    s2.transactions.pending must have size 0
    s2.transactions.accepted(0) mustEqual incomingTransfer.withState(Accepted)
  }

  "in start commit out start commit" should "change balance" in {

    val inTransfer = this.inTransfer.copy(amount = 7.19)
    val outTransfer = this.outTransfer.copy(amount = -1.05)

    val (s1, error1) = AccountReducerSupport.reduce(emptyAccount)(
      TransactionStarted(inTransfer),
      TransactionCommitted(inTransfer.id),
      TransactionStarted(outTransfer),
      TransactionCommitted(outTransfer.id)
    )

    error1 mustBe None

    s1.balance mustEqual 6.14
    s1.transactions.pending must have size 0
    s1.transactions.accepted must have size 2
    s1.transactions.accepted(0) mustEqual outTransfer.withState(Accepted)
    s1.transactions.accepted(1) mustEqual inTransfer.withState(Accepted)
  }

  "in start out start, in commit out commit" should "reject outTransfer" in {
    val inTransfer = this.inTransfer.copy(amount = 7.19)
    val outTransfer = this.outTransfer.copy(amount = -1.05)

    val (s1, error1) = AccountReducerSupport.reduce(emptyAccount)(
      TransactionStarted(inTransfer),
      TransactionStarted(outTransfer),
      TransactionCommitted(inTransfer.id),
      TransactionCommitted(outTransfer.id)
    )

    error1 mustBe Some(BalanceExceeded)

    s1.balance mustEqual 7.19
    s1.transactions.pending must have size 0
    s1.transactions.accepted must have size 1
    s1.transactions.accepted(0) mustEqual inTransfer.withState(Accepted)

    s1.transactions.rejected must have size 1
    s1.transactions.rejected(0) mustEqual outTransfer.withState(Rejected)
  }

  "in start out start, out commit in commit" should "reject outTransfer" in {
    val inTransfer = this.inTransfer.copy(amount = 7.19)
    val outTransfer = this.outTransfer.copy(amount = -1.05)

    val (s1, error1) = AccountReducerSupport.reduce(emptyAccount)(
      TransactionStarted(inTransfer),
      TransactionStarted(outTransfer),
      TransactionCommitted(outTransfer.id),
      TransactionCommitted(inTransfer.id)
    )

    error1 mustBe Some(BalanceExceeded)

    s1.balance mustEqual 7.19
    s1.transactions.pending must have size 0
    s1.transactions.accepted must have size 1
    s1.transactions.accepted(0) mustEqual inTransfer.withState(Accepted)

    s1.transactions.rejected must have size 1
    s1.transactions.rejected(0) mustEqual outTransfer.withState(Rejected)
  }

  "out transaction " should "be idempotent" in {
    val inTransfer = this.inTransfer.copy(amount = 7.19)
    val outTransfer = this.outTransfer.copy(amount = -1.05)

    val (s1, error1) = AccountReducerSupport.reduce(emptyAccount)(
      TransactionStarted(inTransfer),
      TransactionCommitted(inTransfer.id),
      TransactionStarted(outTransfer),
      TransactionCommitted(outTransfer.id),
      TransactionStarted(outTransfer),
      TransactionCommitted(outTransfer.id)
    )

    error1 mustBe None

    s1.balance mustEqual 6.14
    s1.transactions.pending must have size 0
    s1.transactions.accepted must have size 2
    s1.transactions.accepted(0) mustEqual outTransfer.withState(Accepted)
    s1.transactions.accepted(1) mustEqual inTransfer.withState(Accepted)

    s1.transactions.rejected must have size 0
  }

  "in transaction " should "be idempotent" in {
    val inTransfer = this.inTransfer.copy(amount = 7.19)
    val outTransfer = this.outTransfer.copy(amount = -1.05)

    val (s1, error1) = AccountReducerSupport.reduce(emptyAccount)(
      TransactionStarted(inTransfer),
      TransactionCommitted(inTransfer.id),
      TransactionStarted(inTransfer),
      TransactionCommitted(inTransfer.id),
      TransactionStarted(outTransfer),
      TransactionCommitted(outTransfer.id)
    )

    error1 mustBe None

    s1.balance mustEqual 6.14
    s1.transactions.pending must have size 0
    s1.transactions.accepted must have size 2
    s1.transactions.accepted(0) mustEqual outTransfer.withState(Accepted)
    s1.transactions.accepted(1) mustEqual inTransfer.withState(Accepted)

    s1.transactions.rejected must have size 0
  }




  private def makeDeposit(account: Account, deposit: Deposit) = {
    AccountReducerSupport.reduce(account)(TransactionStarted(deposit), TransactionCommitted(deposit.id))
  }

}
