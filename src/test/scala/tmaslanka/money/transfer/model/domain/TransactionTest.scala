package tmaslanka.money.transfer.model.domain

import java.time.Instant

import org.scalatest.FlatSpec
import org.scalatest.Matchers._

class TransactionTest extends FlatSpec {
  import ExampleDomainObjects._

  "Deposit" must "be positive" in {
    def aDeposit(amount: Money) = Deposit(TransactionId.generate, amount, userId, "", Instant.now())
    aDeposit(0.01)

    the [IllegalArgumentException] thrownBy aDeposit(0)
    val e = the [IllegalArgumentException] thrownBy aDeposit(-0.01)
    e.getMessage should include ("Deposit must be positive")
  }

  "Withdrawal" must "be negative" in {
    def aWithdrawal(amount: Money) = Withdrawal(TransactionId.generate, amount, "", userId, Instant.now())
    aWithdrawal(-0.01)

    the [IllegalArgumentException] thrownBy aWithdrawal(0)
    val e = the [IllegalArgumentException] thrownBy aWithdrawal(0.01)
    e.getMessage should include ("Withdrawal must be negative")
  }

  "IncomingTransfer" must "be positive" in {
    def aIncomingTransefer(amount: Money) = IncomingTransfer(TransactionId.generate, accountNumber, amount, "", userId, Instant.now())
    aIncomingTransefer(0.01)

    the [IllegalArgumentException] thrownBy aIncomingTransefer(0)
    val e = the [IllegalArgumentException] thrownBy aIncomingTransefer(-0.01)
    e.getMessage should include ("Incoming transfer must be positive")
  }

  "OutgoingTransfer" must "be negative" in {
    def aOutgoingTransfer(amount: Money) = OutgoingTransfer(TransactionId.generate, accountNumber, amount, "", userId, Instant.now())
    aOutgoingTransfer(-0.01)

    the [IllegalArgumentException] thrownBy aOutgoingTransfer(0)
    val e = the [IllegalArgumentException] thrownBy aOutgoingTransfer(0.01)
    e.getMessage should include ("Outgoing transfer must be negative")
  }
}
