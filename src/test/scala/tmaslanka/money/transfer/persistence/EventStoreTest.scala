package tmaslanka.money.transfer.persistence

import org.scalatest.concurrent.Futures
import org.scalatest.{FlatSpec, MustMatchers}
import tmaslanka.money.transfer.FuturesSupport
import scala.concurrent.ExecutionContext.Implicits.global

class EventStoreTest extends FlatSpec with FuturesSupport with MustMatchers with Futures {
  
  val one = "one"
  val two = "two"
  val three = "three"
  val four = "four"
  
  val oneResult = (List(one), (one, Effect(one)))
  val twoResult = (List(two, one), (two, Effect(two)))
  val threeResult = (List(three, two, one), (three, Effect(three)))
  val fourResult = (List(four, three, two, one), (four, Effect(four)))

  "EventStore" should "load empty events" in new Scope {
    store.loadEvents().await mustBe Vector.empty
    store.loadState().await mustBe List.empty
  }

  "EventStore" should "save one event" in new Scope {
    store.saveEvent(one).await mustBe oneResult

    store.loadEvents().await mustBe Vector(one)
    store.loadState().await mustBe List(one)
  }

  "EventStore" should "save two events" in new Scope {
    store.saveEvents(one, two).await mustBe List(two, one) -> Vector((one, Effect(one)), (two, Effect(two)))

    store.loadEvents().await mustBe Vector(one, two)
    store.loadState().await mustBe List(two, one)
  }

  "EventStore" should "return snapshot in consistent way" in new Scope {
    store.saveEvent(one).await mustBe oneResult
    store.saveEvent(two).await mustBe twoResult
    store.loadState().await mustBe List(two, one)
    store.loadState().await mustBe List(two, one)

    store.saveEvent(three).await mustBe threeResult
    store.saveEvent(four).await mustBe fourResult

    store.loadState().await mustBe List(four, three, two, one)
  }

  "EventStore" should "return events in consistent way" in new Scope {
    store.saveEvent(one).await mustBe oneResult
    store.saveEvent(two).await mustBe twoResult
    store.loadEvents().await mustBe List(one, two)
    store.loadEvents().await mustBe List(one, two)

    store.saveEvent(three).await mustBe threeResult
    store.saveEvent(four).await mustBe fourResult

    store.loadEvents().await mustBe Vector(one, two, three, four)
  }

  case class Effect(s: String)

  class Scope {
    val store = new EventStore[String, List[String], Effect](List(), (state, event) => (event :: state) -> Effect(event), 3)
  }
}
