package tmaslanka.money.transfer.persistence

import org.scalacheck.Gen
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{FlatSpec, MustMatchers}
import tmaslanka.money.transfer.FuturesSupport

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class EventStoreProps extends FlatSpec with GeneratorDrivenPropertyChecks with MustMatchers with FuturesSupport {

  val eventsGen: Gen[List[Int]] = for (
    size <- Gen.choose(8, 1000)
  ) yield (0 until size).toList

  "EventStore" should "save events correctly" in {
    forAll(eventsGen) { events =>
      new Context {
        whenever(events.size > 1) {
          //        println("events: " + events)

          val snapshotsF = events.par
            .map { event =>
              store.saveEvent(event).map(_._1)
            }
          val snapshots = Future.sequence(snapshotsF.toList).await

          val earlierFirst = snapshots.sortBy(_.size)
          //        println("snapshots")
          //        earlierFirst foreach println

          for (List(prev, later) <- earlierFirst.sliding(2)) {
            later.drop(1) mustBe prev
          }

          store.loadState().await.reverse mustBe store.loadEvents().await
        }
      }
    }
  }

  "EventStore" should "save any number of events" in {
    forAll(eventsGen) { events =>
      new Context {
        store.saveEvents(events: _*).await mustEqual events.reverse -> events.map(e => e -> s"$e").toVector

        store.loadState().await mustEqual events.reverse
        store.loadEvents().await mustEqual events
      }
    }
  }

  class Context {
    type SnapshotType = List[Int]
    val store = new EventStore[Int, SnapshotType, String](List.empty, (l, e) => (e :: l) -> s"$e")
  }
}
