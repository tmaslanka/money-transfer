package tmaslanka.money.transfer

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer
import com.typesafe.scalalogging.StrictLogging
import spray.json._

import scala.concurrent.Await
import scala.concurrent.duration._

trait HttpClient extends StrictLogging {

  private val responseTimeout = 3600.seconds
  private val entityTimeout = 1.second

  def settings: Settings

  implicit val system: ActorSystem = ActorSystem("test")
  implicit val mat: ActorMaterializer = ActorMaterializer()


  def request(method: HttpMethod, uri: String, body: String = ""): HttpResponse = {
    val fullUri = s"http://${settings.host}:${settings.port}$uri"
    val request = HttpRequest(method, fullUri, entity = HttpEntity(ContentTypes.`application/json`, body))
    val responseF = Http().singleRequest(request)
    Await.result(responseF, responseTimeout)
  }

  implicit class ResponseOps(response: HttpResponse) {

    def body: String = {
      val responseBody = Await.result(response.entity.toStrict(entityTimeout), entityTimeout).data.utf8String
      logger.info(s"response body: $responseBody")
      responseBody
    }

    def bodyJson: JsValue = body.parseJson

    def bodyAsEntity[T: JsonReader]: T = body.parseJson.convertTo[T]

    def bodyJsonFields: Map[String, JsValue] = bodyJson.asJsObject.fields
  }
}