package tmaslanka.money.transfer

import akka.http.scaladsl.model.HttpMethods.{GET, POST, PUT}
import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import org.scalatest.MustMatchers._
import org.scalatest.{FlatSpec, Inside}
import spray.json._
import tmaslanka.money.transfer.model.domain._
import tmaslanka.money.transfer.model.json.JsonSupport
import tmaslanka.money.transfer.model.messages._
import tmaslanka.money.transfer.server.ServerModule


class ApiTest extends FlatSpec with Inside with RestApi with JsonSupport {
  import tmaslanka.money.transfer.model.ExampleCommands._
  import ExampleDomainObjects._

  val module = new ServerModule()
  val settings = module.settings

  val server = module.server
  server.start()

  "POST to /v1/accounts" should "create an account" in {
    val response = request(method = POST, uri = "/v1/accounts", body = createAccountCommand.asBody)

    response.status mustEqual StatusCodes.OK
    response.bodyAsEntity[CreateAccountCommandResponse].accountNumber.value must not be empty
  }

  "GET to /v1/accounts/unknownAccount" should "return 404 Not Found" in {
    val response = request(method = GET, uri = "/v1/accounts/unknownAccount")

    response.status mustEqual StatusCodes.NotFound
    response.body mustEqual ""
  }

  "PUT to /v1/accounts/unknownAccount/transactions/id" should "return 404" in {
    val response = request(method = PUT, uri = "/v1/accounts/unknownAccount/transactions/id", body = "{}")
    response.status mustEqual StatusCodes.NotFound
  }

  "PUT to /v1/accounts/someAccount/transactions/id" should "return 400 if destination account does not exists" in {
    val account = postAccount()

    makeDeposit(account, amount = 10)

    putTransaction(account, transferTo(unknownAccount, 5))

    val response = putTransaction(account, transferTo(toAccount = unknownAccount, amount = 10))

    response.status mustEqual StatusCodes.BadRequest
    response.bodyAsEntity[MakeAccountTransactionCommandResponse] mustEqual MakeAccountTransactionCommandResponse(Some(AccountNotFound))
  }

  "PUT to /v1/accounts/someAccount/transactions/id" should "return 400 if not enough money on account" in {
    val fromAccount = postAccount()
    val toAccount = postAccount()

    makeDeposit(fromAccount, 5)

    val response = putTransaction(fromAccount, transferTo(toAccount, 10))

    response.status mustEqual StatusCodes.BadRequest
    response.bodyAsEntity[MakeAccountTransactionCommandResponse] mustEqual MakeAccountTransactionCommandResponse(Some(BalanceExceeded))
  }

  "PUT to /v1/accounts/someAccount/transactions/id" should "return 400 if transfer to the same account requested" in {
    val fromAccount = postAccount()

    makeDeposit(fromAccount, 5)

    val response = putTransaction(fromAccount, transferTo(fromAccount, 5))

    println(getTransactions(fromAccount))
    response.status mustEqual StatusCodes.BadRequest
    response.bodyAsEntity[MakeAccountTransactionCommandResponse] mustEqual MakeAccountTransactionCommandResponse(Some(OperationNotAllowed))
  }


  "PUT to /v1/accounts/originAccount/transactions/id" should "return 200" in {
    val fromAccount = postAccount()
    val toAccount = postAccount()

    val (depositId, deposit) = makeDeposit(fromAccount, 10)

    val transferCommand = transferTo(toAccount, 7.50)
    val transfer = transferCommand.transaction.asInstanceOf[MoneyTransferDTO] //todo


    val response = putTransaction(fromAccount, transferCommand)

    response.status mustEqual StatusCodes.OK

    val transactions = getTransactions(fromAccount)
    transactions must have size 2

    val expectedOutTransfer = MoneyTransferDTO(transfer.id, Some(fromAccount), toAccount, -7.50,
      transfer.userId, transfer.description, transactions(1).dateTime, Some(Accepted))

    transactions(0) mustEqual
    DepositDTO(`depositId`, deposit.amount, deposit.userId, deposit.description, transactions(0).dateTime, Some(Accepted))

    transactions(1) mustEqual expectedOutTransfer

    val toAccountTransactions = getTransactions(toAccount)
    toAccountTransactions must have size 1
    toAccountTransactions(0) mustEqual expectedOutTransfer.copy(amount = 7.50)
  }

  "PUT to /v1/accounts/originAccount/transactions/id" should "be idempotent when sending transaction twice" in {
    val fromAccount = postAccount()
    val toAccount = postAccount()

    val (depositId, deposit) = makeDeposit(fromAccount, 20)

    val transferCommand = transferTo(toAccount, 7.50)
    val transfer = transferCommand.transaction.asInstanceOf[MoneyTransferDTO] //todo


    putTransaction(fromAccount, transferCommand).status mustEqual StatusCodes.OK
    val response = putTransaction(fromAccount, transferCommand)

    response.status mustEqual StatusCodes.OK

    val transactions = getTransactions(fromAccount)
    transactions must have size 2

    val expectedOutTransfer = MoneyTransferDTO(transferCommand.id, Some(fromAccount), toAccount, -7.50,
      transfer.userId, transfer.description, transactions(1).dateTime, Some(Accepted))

    transactions(0) mustEqual
    DepositDTO(`depositId`, deposit.amount, deposit.userId, deposit.description, Some(transactions(0).dateTime.get), Some(Accepted))

    transactions(1) mustEqual
      expectedOutTransfer

    val toAccountTransactions = getTransactions(toAccount)
    toAccountTransactions must have size 1
    toAccountTransactions(0) mustEqual expectedOutTransfer.copy(amount = 7.50)
  }

}


trait MessageSupport {

  implicit class JsonOps[T](o: T) {
    def asBody[B >: T : JsonWriter]: String = implicitly[JsonWriter[B]].write(o).toString()
  }
}

trait RestApi extends HttpClient with JsonSupport with MessageSupport {
  import tmaslanka.money.transfer.model.ExampleCommands._

  def postAccount(currency: String = "PLN", owner: String = "123"): AccountNumber = {
    val response = request(
      method = POST,
      uri = "/v1/accounts",
      body = createAccountCommand.copy(
        currency = Currency(currency),
        ownerId = UserId(owner)).asBody)
    response.bodyJson.convertTo[CreateAccountCommandResponse].accountNumber
  }

  def createTransaction(accountNumber: AccountNumber, transaction: MakeAccountTransactionCommand): Unit = {
    val response = putTransaction(accountNumber, transaction)
    response.status mustEqual StatusCodes.OK
  }

  def putTransaction(accountNumber: AccountNumber, transaction: MakeAccountTransactionCommand): HttpResponse = {
    request(
      method = PUT,
      uri = s"/v1/accounts/$accountNumber/transactions/${transaction.id}",
      body = transaction.asBody[MakeAccountTransactionCommand])
  }

  def makeDeposit(account: AccountNumber, amount: Money): (TransactionId, DepositDTO) = {
    val d = depositDto.copy(amount = amount)
    val command = makeDepositCommand.copy(transaction = d)
    putTransaction(account, command)
    command.id -> d
  }

  def getTransactions(accountNumber: AccountNumber): Vector[TransactionDTO] = {
    val response = request(method = GET, uri = s"/v1/accounts/$accountNumber/transactions")
    response.status mustEqual StatusCodes.OK
    response.bodyAsEntity[Vector[TransactionDTO]]
  }
}
