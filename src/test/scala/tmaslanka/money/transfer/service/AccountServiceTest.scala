package tmaslanka.money.transfer.service

import com.typesafe.scalalogging.StrictLogging
import org.scalatest.{BeforeAndAfterAll, FlatSpec, MustMatchers}
import tmaslanka.money.transfer.FuturesSupport
import tmaslanka.money.transfer.model.ExampleCommands
import tmaslanka.money.transfer.model.domain._
import tmaslanka.money.transfer.model.messages._
import tmaslanka.money.transfer.persistence.InMemoryAccountRepository

import scala.concurrent.ExecutionContext.Implicits.global

class AccountServiceTest extends FlatSpec with FuturesSupport with MustMatchers with BeforeAndAfterAll with StrictLogging {
  import tmaslanka.money.transfer.model.ExampleCommands._
  import tmaslanka.money.transfer.model.domain.ExampleDomainObjects._

  "AccountService" should "create account" in new Context {
    val number = service.createAccount(createAccountCommand).await.accountNumber
    val maybeAccount = service.getAccount(GetAccountQuery(number)).await
    maybeAccount must not be empty

    val account = maybeAccount.get
    account.currency mustEqual createAccountCommand.currency
    account.owner mustEqual createAccountCommand.ownerId
    account.balance mustBe 0
  }

  "AccountService deposit money" should "deposit money" in new AccountsContext {

    service.makeTransaction(accountA, makeDepositCommand).await mustEqual MakeAccountTransactionCommandResponse()

    val maybeAccount = service.getAccount(GetAccountQuery(accountANumber)).await
    maybeAccount must not be empty
    val account = maybeAccount.get

    account.balance mustEqual accountA.balance + makeDepositCommand.transaction.amount

    val accepted = service.getTransactions(accountANumber).await
    accepted must have size 1
    accepted(0) must matchPattern {
      case DepositDTO(_, depositDto.amount, depositDto.userId, depositDto.description, _, Some(Accepted)) =>
    }
  }

  "AccountService deposit money" should "return AccountNotFound when account does not exists in repository" in new Context {
    service.makeTransaction(account, makeDepositCommand).await.error mustBe Some(AccountNotFound)
  }

  "AccountService" should "transfer money from a to b" in new AccountsContext {
    val amount: Money = depositDto.amount

    val makeMoneyTransfer = ExampleCommands.makeMoneyTransfer.copy(
      transaction = moneyTransferDto.copy(
        id = TransactionId.generate,
        fromAccount = Some(accountANumber),
        toAccount = accountBNumber,
        amount = amount,
      )
    )

    service.makeTransaction(accountA, makeDepositCommand).await

    val s1 = repo.findAccount(accountANumber).await.get
    repo.findAccount(accountBNumber).await.get

    service.makeTransaction(s1, makeMoneyTransfer).await mustEqual MakeAccountTransactionCommandResponse()

    val aAccepted = service.getTransactions(accountANumber).await
    aAccepted must have size 2

    val negAmount = -amount
    aAccepted(1) must matchPattern {
      case MoneyTransferDTO(id, Some(`accountANumber`), `accountBNumber`, `negAmount`, moneyTransferDto.userId, moneyTransferDto.description, _, Some(Accepted)) =>
    }

    val bAccepted = service.getTransactions(accountBNumber).await
    bAccepted must have size 1

    bAccepted(0) must matchPattern {
      case MoneyTransferDTO(id, Some(`accountANumber`), `accountBNumber`, `amount`, moneyTransferDto.userId, moneyTransferDto.description, _, Some(Accepted)) =>
    }
  }

  "AccountService" should "not transfer money from a to b if not enough money on account" in new AccountsContext {
    val amount: Money = depositDto.amount

    makeMoneyTransfer.copy(
      transaction = moneyTransferDto.copy(
        id = TransactionId.generate,
        amount = amount + 10,
        toAccount = accountBNumber
      )
    )

    service.makeTransaction(accountA, makeDepositCommand).await

    service.makeTransaction(accountA, makeMoneyTransfer).await mustEqual MakeAccountTransactionCommandResponse(Some(BalanceExceeded))

    val aAccepted = service.getTransactions(accountANumber).await
    aAccepted must have size 1

    aAccepted(0) must matchPattern {
      case DepositDTO(_, depositDto.amount, depositDto.userId, depositDto.description, _, Some(Accepted)) =>
    }

    val bAccepted = service.getTransactions(accountBNumber).await
    bAccepted must have size 0
  }

  class Context {
    val repo = new InMemoryAccountRepository(AccountReducer.reduceAccount)
    val service = new AccountService(repo)
  }

  class AccountsContext extends Context {

    val accountANumber = service.createAccount(createAccountCommand).await.accountNumber
    val accountA = repo.findAccount(accountANumber).await.get
    logger.debug(s"Account created $accountA")

    val accountBNumber = service.createAccount(createAccountCommand).await.accountNumber
    val accountB = repo.findAccount(accountBNumber).await.get
    logger.debug(s"Account created $accountB")
  }

}
