package com.tmaslanka.money.transfer

import java.util.UUID
import java.util.concurrent.TimeUnit

import com.typesafe.scalalogging.StrictLogging
import io.restassured.RestAssured._
import io.restassured.module.scala.RestAssuredSupport.AddThenToResponse
import org.hamcrest.Matchers
import org.scalatest.{BeforeAndAfterAll, FlatSpec}
import tmaslanka.money.transfer.model.domain.{AccountNumber, Money}

import scala.annotation.tailrec
import scala.sys.process._
import scala.util.{Failure, Success, Try}

class IntegrationTest extends FlatSpec with BeforeAndAfterAll with StrictLogging {

  var maybeServerProcess: Option[Process] = None

  override protected def beforeAll(): Unit = {
    logger.debug("starting server")
    val process = "java -jar ./target/scala-2.12/money-transfer-assembly.jar".run()

    if (!process.isAlive()) {
      throw new IllegalStateException("server process not started")
    }

    logger.info("Wait for process to start")
    waitConnected()
    logger.info("Connected")

    maybeServerProcess = Some(process)
  }

  override protected def afterAll(): Unit = {
    maybeServerProcess.foreach { serverProcess =>
      logger.debug("stopping server")
      serverProcess.destroy()
    }
  }

  val `application/json` = "application/json"


  "transfer between accounts" should "be successful" in {
    val firstAccount = postAccount()
    val secondAccount = postAccount()

    putDeposit(firstAccount, 100)
      .Then()
      .statusCode(200)


    putTransaction(firstAccount, secondAccount, 50)
      .Then()
      .statusCode(200)

    val response1 = getTransactions(firstAccount)
    response1
      .Then()
      .statusCode(200)
      .body("", Matchers.hasSize(2))
      .body("[0].amount", Matchers.equalTo(100))
      .body("[0].type", Matchers.equalTo("deposit"))
      .body("[1].amount", Matchers.equalTo(-50))
      .body("[1].type", Matchers.equalTo("transfer"))


    val response2 = getTransactions(secondAccount)
    response2
      .Then()
      .statusCode(200)
      .body("", Matchers.hasSize(1))
      .body("[0].amount", Matchers.equalTo(50))
      .body("[0].type", Matchers.equalTo("transfer"))

    val response1Body = response1.getBody.prettyPrint()
    val response2Body =response2.getBody.prettyPrint()

    logger.debug(s"From account [$firstAccount] transactions: $response1Body")
    logger.debug(s"To account [$secondAccount] transactions: $response2Body")
  }

  "transfer between accounts" should "return balance exceeded" in {
    val firstAccount = postAccount()
    val secondAccount = postAccount()

    putDeposit(firstAccount, 100)
      .Then()
      .statusCode(200)


    putTransaction(firstAccount, secondAccount, 150)
      .Then()
      .statusCode(400)
      .body("error", Matchers.equalTo("BalanceExceeded"))

    val response1 = getTransactions(firstAccount)
    response1
      .Then()
      .statusCode(200)
      .body("", Matchers.hasSize(1))
      .body("[0].amount", Matchers.equalTo(100))
      .body("[0].type", Matchers.equalTo("deposit"))


    val response2 = getTransactions(secondAccount)
    response2
      .Then()
      .statusCode(200)
      .body("", Matchers.hasSize(0))

    val response1Body = response1.getBody.prettyPrint()
    val response2Body =response2.getBody.prettyPrint()

    logger.debug(s"From account [$firstAccount] transactions: $response1Body")
    logger.debug(s"To account [$secondAccount] transactions: $response2Body")
  }

  private def getTransactions(accountNumber: AccountNumber) = {
    given()
      .get(s"v1/accounts/${accountNumber.value}/transactions")
  }

  private def putTransaction(fromAccount: AccountNumber, toAccount: AccountNumber, amount: Money) = {
    val id = UUID.randomUUID().toString
    given()
      .contentType(`application/json`)
      .body(
        s"""{
           |  "id": "$id",
           |  "toAccount": "${toAccount.value}",
           |  "amount": "$amount",
           |  "userId": "123",
           |  "description": "some transfer",
           |  "type": "transfer"
           } """.stripMargin)
      .put(s"v1/accounts/${fromAccount.value}/transactions/$id")
  }

  private def postAccount() = {
    AccountNumber(
      given()
        .contentType(`application/json`)
        .body(""" {"currency": "PLN", "ownerId": "123"} """)
        .when()
        .post("v1/accounts")
        .body()
        .path("accountNumber"))
  }

  private def putDeposit(accountNumber: AccountNumber, amount: Money) = {
    val id = UUID.randomUUID().toString
    given()
      .contentType(`application/json`)
      .body(
        s"""{
          |  "id": "$id",
          |  "amount": "$amount",
          |  "userId": "123",
          |  "description": "deposit 1",
          |  "type": "deposit"
           } """.stripMargin)
      .put(s"v1/accounts/${accountNumber.value}/transactions/$id")
  }

  @tailrec
  private def waitConnected(statusCode: Try[Int] = Failure(new RuntimeException()),
                            count: Int = 0): Int = {
    statusCode match {
      case Failure(exception) if count == 100 =>
        throw new RuntimeException("Can not connect to docker container", exception)
      case Failure(_) =>
        TimeUnit.MILLISECONDS.sleep(100)
        waitConnected(Try(get("v1/accounts").andReturn().statusCode()), count + 1)
      case Success(value) =>
        value
    }
  }
}