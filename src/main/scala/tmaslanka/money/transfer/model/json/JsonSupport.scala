package tmaslanka.money.transfer.model.json

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{DefaultJsonProtocol, JsString, JsValue, JsonFormat, deserializationError}
import tmaslanka.money.transfer.model.StringValue

import scala.reflect.ClassTag

trait JsonSupport
  extends ModelJsonSupport
  with MessagesJsonSupport




trait BaseJsonSupport extends SprayJsonSupport with DefaultJsonProtocol with DateTimeJsonSupport {

  protected def valueJsonFormat[T <: Product with StringValue : ClassTag](create: String => T): JsonFormat[T] = new JsonFormat[T] {
    override def write(obj: T): JsValue = {
      JsString(obj.value)
    }
    override def read(json: JsValue): T = json match {
      case JsString(v) => create(v)
      case _ => deserializationError("String expected")
    }
  }


  trait AdtSupport {
    val discriminatorName: String = "type"

    def readDiscriminator(json: JsValue): String = {
      json.asJsObject.fields.get(discriminatorName) match {
        case Some(JsString(value)) => value
        case _ => deserializationError(s"No expected field $discriminatorName found.")
      }
    }

    def withDiscriminator[T: JsonFormat](obj: T, discriminator: String): JsValue = {
      val jsObject = implicitly[JsonFormat[T]].write(obj).asJsObject
      jsObject.copy(fields = jsObject.fields + (discriminatorName -> JsString(discriminator)))
    }
  }
}
