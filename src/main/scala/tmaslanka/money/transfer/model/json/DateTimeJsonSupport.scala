package tmaslanka.money.transfer.model.json

import java.time.Instant
import java.time.format.DateTimeFormatter

import spray.json.{JsString, JsValue, JsonFormat, deserializationError}

trait DateTimeJsonSupport {
  implicit val instantFormat = new JsonFormat[Instant] {
    override def write(obj: Instant): JsValue = JsString(obj.toString)

    override def read(json: JsValue): Instant = json match {
      case JsString(value) => Instant.from(DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse(value))
      case _ => deserializationError("ISO date time expected")
    }
  }
}
