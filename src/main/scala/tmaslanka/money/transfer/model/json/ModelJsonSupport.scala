package tmaslanka.money.transfer.model.json

import spray.json.{JsString, JsValue, RootJsonFormat, deserializationError}
import tmaslanka.money.transfer.model.domain._

trait ModelJsonSupport extends BaseJsonSupport {

  implicit val userIdFormat = valueJsonFormat(UserId)
  implicit val accountNumberFormat = valueJsonFormat(AccountNumber.apply)
  implicit val currencyFormat = valueJsonFormat(Currency)
  implicit val transactionIdFormat = valueJsonFormat(TransactionId.apply)

  implicit val transactionStateFormat = new RootJsonFormat[TransactionState] {
    private val pending = "pending"
    private val accepted = "accepted"
    private val rejected = "rejected"

    override def write(obj: TransactionState): JsValue = JsString (obj match {
      case Pending => pending
      case Accepted => accepted
      case Rejected => rejected
    })

    override def read(json: JsValue): TransactionState = json match {
      case JsString(value) => value match {
        case `pending` => Pending
        case `accepted` => Accepted
        case `rejected` => Rejected
        case _ => deserializationError("Expected transaction state value")
      }
      case _ => deserializationError("Expected string value")
    }
  }

  implicit val transactionFormat: RootJsonFormat[Transaction] = new RootJsonFormat[Transaction] with AdtSupport {

    private implicit val depositFormat = jsonFormat6(Deposit)
    private implicit val withdrawalFormat = jsonFormat6(Withdrawal)
    private implicit val incomingTransferFormat = jsonFormat7(IncomingTransfer)
    private implicit val outgoingTransferFormat = jsonFormat7(OutgoingTransfer)

    private val deposit = "deposit"
    private val withdrawal = "withdrawal"
    private val incomingTransfer = "IncomingTransfer"
    private val outgoingTransfer = "OutgoingTransfer"

    override def write(obj: Transaction): JsValue = obj match {
      case d: Deposit => withDiscriminator(d, deposit)
      case w: Withdrawal => withDiscriminator(w, withdrawal)
      case it: IncomingTransfer => withDiscriminator(it, incomingTransfer)
      case ot: OutgoingTransfer => withDiscriminator(ot, outgoingTransfer)
    }


    override def read(json: JsValue): Transaction = {
      readDiscriminator(json) match {
        case `deposit` => json.convertTo[Deposit]
        case `withdrawal` => json.convertTo[Withdrawal]
        case `incomingTransfer` => json.convertTo[IncomingTransfer]
        case `outgoingTransfer` => json.convertTo[OutgoingTransfer]
      }
    }
  }
}
