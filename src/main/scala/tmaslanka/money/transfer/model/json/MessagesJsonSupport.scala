package tmaslanka.money.transfer.model.json

import spray.json.{JsString, JsValue, JsonFormat, RootJsonFormat, deserializationError}
import tmaslanka.money.transfer.model.messages._

trait MessagesJsonSupport extends BaseJsonSupport with ModelJsonSupport {

  implicit val getAccountQueryFormat = jsonFormat1(GetAccountQuery)
  implicit val getAccountQueryResponseFormat = jsonFormat4(GetAccountQueryResponse)

  implicit val createAccountRequestFormat = jsonFormat2(CreateAccountCommand)
  implicit val createAccountResponseFormat = jsonFormat1(CreateAccountCommandResponse)

  implicit val accountTransactionDtoFormat = new RootJsonFormat[TransactionDTO] with AdtSupport {
    implicit val depositFormat = jsonFormat6(DepositDTO)
    implicit val moneyTransferFormat = jsonFormat8(MoneyTransferDTO)

    override def write(obj: TransactionDTO): JsValue = obj match {
      case d: DepositDTO => withDiscriminator(d, "deposit")
      case mt: MoneyTransferDTO => withDiscriminator(mt, "transfer")
    }

    override def read(json: JsValue): TransactionDTO = {
      readDiscriminator(json) match {
        case "deposit" => depositFormat.read(json)
        case "transfer" => moneyTransferFormat.read(json)
      }
    }
  }

  implicit val transactionErrorFormat = new JsonFormat[TransactionError] with AdtSupport {
    override def write(obj: TransactionError): JsValue = JsString( obj match {
      case BalanceExceeded => "BalanceExceeded"
      case AccountNotFound => "AccountNotFound"
      case OperationNotAllowed => "OperationNotAllowed"
    })

    override def read(json: JsValue): TransactionError = json match {
      case JsString(value) => value match {
        case "BalanceExceeded" => BalanceExceeded
        case "AccountNotFound" => AccountNotFound
        case "OperationNotAllowed" => OperationNotAllowed
      }
      case _ => deserializationError("String expected")
    }
  }

  implicit val makeAccountTransactionCommandFormat = new RootJsonFormat[MakeAccountTransactionCommand] {
    override def write(obj: MakeAccountTransactionCommand): JsValue =
      accountTransactionDtoFormat.write(obj.transaction)
    override def read(json: JsValue): MakeAccountTransactionCommand =
      MakeAccountTransactionCommand(accountTransactionDtoFormat.read(json))
  }
  implicit val makeAccountTransactionCommandResponseFormat = jsonFormat1((e: Option[TransactionError]) => MakeAccountTransactionCommandResponse(e))
}
