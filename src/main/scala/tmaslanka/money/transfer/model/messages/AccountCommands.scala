package tmaslanka.money.transfer.model.messages

import tmaslanka.money.transfer.model.domain._

final case class CreateAccountCommand(currency: Currency, ownerId: UserId) {
  def toGeneratedAccount: Account = {
    val accountNumber = AccountNumber.generate
    Account(accountNumber, ownerId, currency)
  }
}

final case class CreateAccountCommandResponse(accountNumber: AccountNumber)



final case class MakeAccountTransactionCommand(transaction: TransactionDTO) {
  def id: TransactionId = transaction.id
}

final case class MakeAccountTransactionCommandResponse(error: Option[TransactionError] = None)

object MakeAccountTransactionCommandResponse {
  def apply(error: TransactionError): MakeAccountTransactionCommandResponse = MakeAccountTransactionCommandResponse(Some(error))
}