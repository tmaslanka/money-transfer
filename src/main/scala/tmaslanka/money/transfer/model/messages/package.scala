package tmaslanka.money.transfer.model

import java.time.Instant

import tmaslanka.money.transfer.model.domain._

package object messages {
  def dtoToTransaction(cmd: MakeAccountTransactionCommand, now: Instant): Transaction = cmd.transaction match {
    case DepositDTO(id, amount, userId, description, _, _) =>
      Deposit(id, amount, userId, description, now, Pending)

    case MoneyTransferDTO(id, fromAccount, toAccount, amount, userId, description, _, _) =>
      OutgoingTransfer(id, toAccount, -amount, description, userId, now, Pending)
  }

  def transactionToDto(account: AccountNumber)(transaction: Transaction): TransactionDTO = transaction match {
    case Deposit(id, amount, userId, description, dateTime, state) =>
      DepositDTO(id, amount, userId, description, Some(dateTime), Some(state))

    case Withdrawal(id, amount, description, userId, dateTime, state) =>
      ???

    case IncomingTransfer(id, fromAccount, amount, description, userId, dateTime, state) =>
      MoneyTransferDTO(id, Some(fromAccount), account, amount, userId, description, Some(dateTime), Some(state))

    case OutgoingTransfer(id, toAccount, amount, description, userId, dateTime, state) =>
      MoneyTransferDTO(id, Some(account), toAccount, amount, userId, description, Some(dateTime), Some(state))
  }
}
