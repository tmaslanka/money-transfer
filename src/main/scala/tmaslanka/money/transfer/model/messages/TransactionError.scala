package tmaslanka.money.transfer.model.messages

sealed trait TransactionError
case object BalanceExceeded extends TransactionError
case object AccountNotFound extends TransactionError
case object OperationNotAllowed extends TransactionError
