package tmaslanka.money.transfer.model.messages

import tmaslanka.money.transfer.model.domain._

final case class GetAccountQuery(accountNumber: AccountNumber)
final case class GetAccountQueryResponse(accountNumber: AccountNumber,
                                         owner: UserId,
                                         currency: Currency,
                                         balance: Money)