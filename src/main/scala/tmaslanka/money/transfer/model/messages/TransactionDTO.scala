package tmaslanka.money.transfer.model.messages

import java.time.Instant

import tmaslanka.money.transfer.model.domain._

sealed trait TransactionDTO {
  def id: TransactionId
  def amount: Money
  def userId: UserId
  def description: String
  def dateTime: Option[Instant]
  def state: Option[TransactionState]
}

final case class DepositDTO(id: TransactionId,
                            amount: Money,
                            userId: UserId,
                            description: String,
                            dateTime: Option[Instant] = None,
                            state: Option[TransactionState] = None) extends TransactionDTO

final case class MoneyTransferDTO(id: TransactionId,
                                  fromAccount: Option[AccountNumber],
                                  toAccount: AccountNumber,
                                  amount: Money,
                                  userId: UserId,
                                  description: String,
                                  dateTime: Option[Instant] = None,
                                  state: Option[TransactionState] = None) extends TransactionDTO
