package tmaslanka.money.transfer.model

trait StringValue {
  def value: String
  override def toString: String = value
}
