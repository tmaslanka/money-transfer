package tmaslanka.money.transfer.model.domain

sealed trait TransactionEvent {
  def transactionId: TransactionId
}

case class TransactionStarted(transaction: Transaction) extends TransactionEvent {
  override def transactionId: TransactionId = transaction.id
}

case class TransactionCommitted(transactionId: TransactionId) extends TransactionEvent