package tmaslanka.money.transfer.model.domain

import java.time.Instant
import java.util.UUID

import tmaslanka.money.transfer.model.StringValue

final case class TransactionId(value: String) extends StringValue

object TransactionId {
  def generate: TransactionId = TransactionId(UUID.randomUUID().toString)
}

sealed trait TransactionState
case object Pending extends TransactionState
case object Accepted extends TransactionState
case object Rejected extends TransactionState

sealed trait Transaction {
  def id: TransactionId
  def amount: Money
  def dateTime: Instant
  def state: TransactionState

  def withState(state: TransactionState): Transaction
}

final case class Deposit(id: TransactionId, amount: Money, userId: UserId, description: String, dateTime: Instant, state: TransactionState = Pending) extends Transaction {
  require(amount > 0, "Deposit must be positive")

  override def withState(state: TransactionState): Transaction = copy(state = state)
}

final case class Withdrawal(id: TransactionId,
                            amount: Money,
                            description: String,
                            userId: UserId,
                            dateTime: Instant,
                            state: TransactionState = Pending) extends Transaction {
  require(amount < 0, "Withdrawal must be negative")

  override def withState(state: TransactionState): Transaction = copy(state = state)

}

final case class IncomingTransfer(id: TransactionId,
                                  fromAccount: AccountNumber,
                                  amount: Money,
                                  description: String,
                                  userId: UserId,
                                  dateTime: Instant,
                                  state: TransactionState = Pending) extends Transaction {
  require(amount > 0, "Incoming transfer must be positive")

  override def withState(state: TransactionState): Transaction = copy(state = state)

}

final case class OutgoingTransfer(id: TransactionId,
                                  toAccount: AccountNumber,
                                  amount: Money,
                                  description: String,
                                  userId: UserId,
                                  dateTime: Instant,
                                  state: TransactionState = Pending) extends Transaction {
  def toIncoming(fromAccount: AccountNumber): IncomingTransfer =
    IncomingTransfer(id, fromAccount, -amount, description, userId, dateTime, state)

  require(amount < 0, "Outgoing transfer must be negative")

  override def withState(state: TransactionState): Transaction = copy(state = state)

}