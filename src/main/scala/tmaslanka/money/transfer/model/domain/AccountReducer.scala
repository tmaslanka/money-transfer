package tmaslanka.money.transfer.model.domain

import tmaslanka.money.transfer.model.messages.{BalanceExceeded, TransactionError}

object AccountReducer {
  def reduceAccount(account: Account, event: TransactionEvent): (Account, Option[TransactionError]) = event match {
    case TransactionStarted(transaction) if exists(account, transaction) =>
      account -> None

    case TransactionStarted(transaction) =>
      if (transaction.amount < 0) {
        tryStartOutTransaction(account, transaction)
      } else {
        startInTransaction(account, transaction)
      }

    case TransactionCommitted(id) =>
      commitTransaction(account, id) -> None
  }

  private def commitTransaction(account: Account, id: TransactionId) = {
    account.transactions.pending.get(id).map { tr =>
      account
        .modifyBlocked(blocked => if (tr.amount < 0) blocked + tr.amount else blocked)
        .modifyBalance(balance => if (tr.amount > 0) balance + tr.amount else balance)
        .modifyTransactions(_.withAcceptedTransaction(id))
    }.getOrElse(account)
  }

  private def tryStartOutTransaction(account: Account, transaction: Transaction) = {
    val available = account.balance + transaction.amount
    if (available < 0) {
      account.withTransactionAdded(transaction.withState(Rejected)) -> Some(BalanceExceeded)
    } else {
      account
        .modifyBlocked(_ => account.blocked - transaction.amount)
        .modifyBalance(_ + transaction.amount)
        .withTransactionAdded(transaction.withState(Pending)) -> None
    }
  }

  private def startInTransaction(account: Account, transaction: Transaction) = {
    account.withTransactionAdded(transaction.withState(Pending)) -> None
  }

  private def exists(account: Account, transaction: Transaction) = {
    account.transactions.toVector.exists(_.id == transaction.id)
  }
}
