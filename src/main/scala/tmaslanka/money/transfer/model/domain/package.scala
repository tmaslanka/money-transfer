package tmaslanka.money.transfer.model

package object domain {
  type Money = BigDecimal
  type Sequence = Long
}
