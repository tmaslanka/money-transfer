package tmaslanka.money.transfer.model.domain

import tmaslanka.money.transfer.model.StringValue

final case class UserId(value: String) extends StringValue
