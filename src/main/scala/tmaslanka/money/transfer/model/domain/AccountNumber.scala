package tmaslanka.money.transfer.model.domain

import tmaslanka.money.transfer.model.StringValue

import scala.util.Random

object AccountNumber {
  def generate: AccountNumber = AccountNumber(Random.nextLong().abs.toString)
}

final case class AccountNumber(value: String) extends StringValue
