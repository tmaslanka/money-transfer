package tmaslanka.money.transfer.model.domain

case class Account(number: AccountNumber,
                   ownerId: UserId,
                   currency: Currency,
                   balance: Money = 0,
                   blocked: Money = 0,
                   transactions: Transactions = Transactions()) {

  def withTransactionAdded(transaction: Transaction): Account =
    modifyTransactions(_.withTransaction(transaction))

  def modifyTransactions(update: Transactions => Transactions): Account =
    copy(transactions = update(transactions))

  def modifyBalance(update: Money => Money): Account = copy(balance = update(balance))

  def modifyBlocked(update: Money => Money): Account = copy(blocked = update(blocked))
}

final case class Transactions(history: Vector[Transaction] = Vector(),
                              pending: Map[TransactionId, Transaction] = Map()) {


  def accepted: Vector[Transaction] = history.filter(_.state == Accepted)

  def rejected: Vector[Transaction] = history.filter(_.state == Rejected)

  def withTransaction(transaction: Transaction): Transactions = {
    transaction.state match {
      case Accepted => copy(history = transaction +: history)
      case Pending => copy(pending = pending.updated(transaction.id, transaction))
      case Rejected => copy(history = transaction +: history)
    }
  }

  def withAcceptedTransaction(id: TransactionId): Transactions = {
    pending.get(id) match {
      case Some(tr) => copy(pending = pending - id, history = tr.withState(Accepted) +: history)
      case None => this
    }
  }

  def toVector: Vector[Transaction] = pending.values ++: history
}