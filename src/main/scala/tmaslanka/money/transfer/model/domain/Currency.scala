package tmaslanka.money.transfer.model.domain

import tmaslanka.money.transfer.model.StringValue

final case class Currency(value: String) extends StringValue
