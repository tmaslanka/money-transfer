package tmaslanka.money.transfer.persistence

import tmaslanka.money.transfer.model.domain._

import scala.concurrent.Future


trait AccountRepository {

  def findAccountNumbers: Future[Seq[AccountNumber]]

  def createAccount(account: Account): Future[Boolean]

  def findAccount(accountNumber: AccountNumber): Future[Option[Account]]

  def findAllTransactions(accountNumber: AccountNumber): Future[Vector[Transaction]]

  def findTransaction(account: AccountNumber, id: TransactionId): Future[Option[Transaction]]

  def eventsSaver(accountNumber: AccountNumber): Future[Option[EventSaver]]
}
