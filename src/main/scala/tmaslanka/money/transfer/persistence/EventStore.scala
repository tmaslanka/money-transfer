package tmaslanka.money.transfer.persistence

import java.util.concurrent.atomic.AtomicReference

import scala.concurrent.{ExecutionContext, Future}

class EventStore[Event, S, Effect](s0: S, reduce: (S, Event) => (S, Effect), saveSnapshotEvery: Int = 10)
                          (implicit ex: ExecutionContext) {

  private val savedEvents: AtomicReference[Vector[Event]] = new AtomicReference(Vector())
  private val snapshot: AtomicReference[(Int, S)] = new AtomicReference((-1, s0))

  def saveEvent(event: Event): Future[(S, (Event, Effect))] = saveEvents(event).map {case (s, es) if es.size == 1 => s -> es(0)}

  def saveEvents(events: Event*): Future[(S, Vector[(Event, Effect)])] = Future.successful {
    val eventsVector = events.toVector
    val newEvents = savedEvents.updateAndGet(es => es ++ eventsVector)

    saveSnapshot(newEvents)

    val loadedState = loadState(newEvents.dropRight(eventsVector.size))
    eventsVector.foldLeft(loadedState -> Vector.empty[(Event, Effect)])(reduceWithEffects)
  }

  def loadEvents(): Future[Vector[Event]] = Future.successful(savedEvents.get())

  def loadState(): Future[S] = Future.successful {
    val es = savedEvents.get
    val (s1Seq, s1) = snapshot.get
    val s2Seq = es.size - 1
    if(s2Seq > s1Seq) {
      es.takeRight(s2Seq - s1Seq).foldLeft(s1)(reduceStateOnly)
    } else {
      s1
    }
  }

  private def saveSnapshot(events: Vector[Event]): Unit = {
    val eventSeq = events.size-1
    if (eventSeq % saveSnapshotEvery == 0) {
      snapshot.synchronized {
        val (seq, s1) = snapshot.get
        if(seq < eventSeq) {
          val s2 = events.takeRight(eventSeq - seq).foldLeft(s1)(reduceStateOnly)
          snapshot.set(eventSeq -> s2)
        }
      }
    }
  }

  private def loadState(newEvents: Vector[Event]): S = {
    val eventSeq = newEvents.size - 1
    val (seq, s) = snapshot.get
    val eventSnapshot =
      if (seq == eventSeq) {
        s
      } else if (seq < eventSeq) {
        newEvents.takeRight(eventSeq - seq).foldLeft(s)(reduceStateOnly)
      } else {
        newEvents.foldLeft(s0)(reduceStateOnly)
      }
    eventSnapshot
  }

  private def reduceStateOnly(s: S, e: Event): S = reduce(s, e)._1

  private def reduceWithEffects(stateWithEffects: (S, Vector[(Event, Effect)]), e: Event): (S, Vector[(Event, Effect)]) = {
    val (s0, eventsWithEffects) = stateWithEffects
    val (s1, effect) = reduce(s0, e)
    s1 -> (eventsWithEffects :+ e -> effect)
  }
}