package tmaslanka.money.transfer.persistence

import tmaslanka.money.transfer.model.domain.{Account, TransactionEvent}
import tmaslanka.money.transfer.model.messages.TransactionError

import scala.concurrent.{ExecutionContext, Future}

case class SaveTransactionEventsResult(account: Account, events: Vector[(TransactionEvent, Option[TransactionError])]) {
  def firstError: Option[TransactionError] = events.map(_._2).collectFirst {case e@Some(_) => e}.flatten
}

case class SaveTransactionEventResult(account: Account, event: TransactionEvent, maybeError: Option[TransactionError])

trait EventSaver {
  def saveEvents(events: TransactionEvent*): Future[SaveTransactionEventsResult]

  def saveEvent(event: TransactionEvent)(implicit ex: ExecutionContext): Future[SaveTransactionEventResult] = {
    saveEvents(event).map {
      case SaveTransactionEventsResult(account, Vector((e, maybeError))) =>
        SaveTransactionEventResult(account, e, maybeError)
    }
  }
}
