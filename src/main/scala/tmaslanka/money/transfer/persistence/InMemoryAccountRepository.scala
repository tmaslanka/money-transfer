package tmaslanka.money.transfer.persistence
import java.util.concurrent.ConcurrentHashMap

import com.typesafe.scalalogging.StrictLogging
import tmaslanka.money.transfer.model.domain._
import tmaslanka.money.transfer.model.messages.TransactionError

import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Future}


class InMemoryAccountRepository(accountReducer: (Account, TransactionEvent) => (Account, Option[TransactionError]))(implicit ex: ExecutionContext)
  extends AccountRepository with StrictLogging {

  private val accounts = new ConcurrentHashMap[AccountNumber, EventStore[TransactionEvent, Account, Option[TransactionError]]]().asScala


  override def findAccountNumbers: Future[Seq[AccountNumber]] = Future.successful {
    accounts.keySet.toVector
  }

  override def createAccount(account: Account): Future[Boolean] = Future.successful {
    accounts.putIfAbsent(account.number, new EventStore(account, accountReducer)).isEmpty
  }

  override def findAccount(accountNumber: AccountNumber): Future[Option[Account]] = {
    accounts.get(accountNumber) match {
      case Some(store) =>
        store
          .loadState()
          .map(Some.apply)
      case None => Future.successful(None)
    }
  }

  override def findAllTransactions(accountNumber: AccountNumber): Future[Vector[Transaction]] = {
    accounts.get(accountNumber) match {
      case Some(store) =>
        logger.debug(s"Account[$accountNumber] event store found")
        store.loadState().map { state =>
          logger.debug(s"Account[$accountNumber] state $state")
          state.transactions.toVector
        }
      case None =>
        Future.successful(Vector())
    }
  }

  override def findTransaction(accountNumber: AccountNumber, id: TransactionId): Future[Option[Transaction]] = {
    accounts.get(accountNumber) match {
      case Some(store) =>
        store.loadState().map(_.transactions.toVector.find(_.id == id))
      case None => Future.successful(None)
    }
  }

  override def eventsSaver(accountNumber: AccountNumber): Future[Option[EventSaver]] = Future.successful {
    val maybeEventStore = accounts.get(accountNumber)
    logger.debug(s"EventStore for $accountNumber is $maybeEventStore")
    maybeEventStore.map { store =>
      val saver: EventSaver = new EventSaver {
        override def saveEvents(events: TransactionEvent*): Future[SaveTransactionEventsResult] = {
          store.saveEvents(events: _*).map(SaveTransactionEventsResult.tupled)
        }
      }
      saver
    }
  }
}
