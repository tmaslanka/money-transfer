package tmaslanka.money.transfer

import com.typesafe.config.Config

case class Settings(config: Config) {
  val host: String = config.getString("money-transfer.server.host")
  val port: Int = config.getInt("money-transfer.server.port")
}