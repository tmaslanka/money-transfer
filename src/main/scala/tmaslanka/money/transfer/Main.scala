package tmaslanka.money.transfer

import tmaslanka.money.transfer.server.ServerModule

object Main extends App {
  val module = new ServerModule()
  module.server.start()
}
