package tmaslanka.money.transfer.service

import java.time.Instant

import com.typesafe.scalalogging.StrictLogging
import tmaslanka.money.transfer.model.domain._
import tmaslanka.money.transfer.model.messages._
import tmaslanka.money.transfer.persistence.{AccountRepository, EventSaver, SaveTransactionEventResult, SaveTransactionEventsResult}

import scala.concurrent.{ExecutionContext, Future}

class AccountService(repo: AccountRepository)(implicit ex: ExecutionContext) extends StrictLogging {

  private val transactionsMaker = new TransactionsMaker(repo)

  def findAccountNumbers: Future[Seq[AccountNumber]] = repo.findAccountNumbers

  def createAccount(command: CreateAccountCommand): Future[CreateAccountCommandResponse] = {
    val account = Account(AccountNumber.generate, command.ownerId, command.currency)
    repo.createAccount(account).map { created =>
      if (created) {
        CreateAccountCommandResponse(account.number)
      } else {
        throw new IllegalStateException("Unexpected state when trying to create an account")
      }
    }
  }

  def getAccount(query: GetAccountQuery): Future[Option[GetAccountQueryResponse]] = {
    repo.findAccount(query.accountNumber).map(_.map { account =>
      GetAccountQueryResponse(account.number, account.ownerId, account.currency, account.balance)
    })
  }

  def makeTransaction(account: Account, command: MakeAccountTransactionCommand): Future[MakeAccountTransactionCommandResponse] = {
    logger.debug(s"makeTransactions $command")
    transactionsMaker.makeTransaction(account, command)
  }

  def findTransaction(accountNumber: AccountNumber, transactionId: TransactionId): Future[Option[TransactionDTO]] = {
    repo.findTransaction(accountNumber, transactionId).map(_.map(transactionToDto(accountNumber)))
  }

  def getTransactions(accountNumber: AccountNumber): Future[Vector[TransactionDTO]] = {
    repo.findAllTransactions(accountNumber).map(_.reverse).map(_.map(transactionToDto(accountNumber)))
  }
}

private[service] class TransactionsMaker(repo: AccountRepository)(implicit ex: ExecutionContext) extends StrictLogging {

  val balanceExceeded = MakeAccountTransactionCommandResponse(BalanceExceeded)

  def makeTransaction(account: Account, command: MakeAccountTransactionCommand): Future[MakeAccountTransactionCommandResponse] = {
    logger.debug(s"Making transaction ${command.transaction.id}, amount: ${command.transaction.amount}")
    val number = account.number
    val response = dtoToTransaction(command, Instant.now) match {
      case deposit: Deposit =>
        eventsSaver(number) flatMap {
          _.saveEvents(TransactionStarted(deposit), TransactionCommitted(deposit.id))
            .map(toResponse)
        }

      case transfer: OutgoingTransfer =>
        makeTransfer(account, transfer)

      case _ =>
        Future.successful(MakeAccountTransactionCommandResponse(OperationNotAllowed))
    }

    response.recover {
      case TransactionException(error) => MakeAccountTransactionCommandResponse(Some(error))
    }
  }

  private def makeTransfer(account: Account, transfer: OutgoingTransfer): Future[MakeAccountTransactionCommandResponse] = {
    val fromAccount = account.number

    if(fromAccount == transfer.toAccount) {
      Future.successful(MakeAccountTransactionCommandResponse(OperationNotAllowed))

    } else if (account.balance + transfer.amount < 0) {
      Future.successful(balanceExceeded)

    } else {
      for {
        fromAccountSaver <- eventsSaver(fromAccount)
        toAccountSaver <- eventsSaver(transfer.toAccount)
        started <- fromAccountSaver.saveEvent(TransactionStarted(transfer))
        response <- {
          if (started.maybeError.isDefined) {
            Future.successful(toResponse(started))
          } else {
            val incomingTransfer = transfer.toIncoming(fromAccount)
            commit(fromAccountSaver, toAccountSaver, incomingTransfer)
          }
        }
      } yield response
    }
  }

  def commit(fromAccountSaver: EventSaver, toAccountSaver: EventSaver,
             incoming: IncomingTransfer): Future[MakeAccountTransactionCommandResponse] = {
    val start = TransactionStarted(incoming)
    val commit = TransactionCommitted(incoming.id)

    toAccountSaver.saveEvents(start, commit) flatMap { incomingResult =>
      if(incomingResult.firstError.isDefined) {
        Future.successful(toResponse(incomingResult))
      } else {
        fromAccountSaver.saveEvent(TransactionCommitted(incoming.id)).map(toResponse)
      }
    }
  }

  private def eventsSaver[A](accountNumber: AccountNumber): Future[EventSaver] = {
    repo.eventsSaver(accountNumber) map {
      case None =>
        throw TransactionException(AccountNotFound)
      case Some(saver) =>
        saver
    }
  }

  private def toResponse(save: SaveTransactionEventResult) =
    MakeAccountTransactionCommandResponse(save.maybeError)

  private def toResponse(save: SaveTransactionEventsResult) =
    MakeAccountTransactionCommandResponse(save.firstError)
}

final case class TransactionException(error: TransactionError) extends RuntimeException