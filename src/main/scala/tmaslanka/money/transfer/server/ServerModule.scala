package tmaslanka.money.transfer.server

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import tmaslanka.money.transfer.Settings
import tmaslanka.money.transfer.model.domain.AccountReducer
import tmaslanka.money.transfer.persistence.InMemoryAccountRepository
import tmaslanka.money.transfer.service.AccountService

import scala.concurrent.ExecutionContext

class ServerModule {
  implicit val system: ActorSystem = ActorSystem("server")
  implicit val mat: ActorMaterializer = ActorMaterializer()
  implicit val ex: ExecutionContext = system.dispatcher

  val settings = Settings(ConfigFactory.load())

  val accountRepository = new InMemoryAccountRepository(AccountReducer.reduceAccount)

  val accountService = new AccountService(accountRepository)

  val server = new Server(settings, accountService, accountRepository)
}
