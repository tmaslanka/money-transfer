package tmaslanka.money.transfer.server

import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.Materializer
import com.typesafe.scalalogging.StrictLogging
import tmaslanka.money.transfer.Settings
import tmaslanka.money.transfer.model.domain.{AccountNumber, TransactionId}
import tmaslanka.money.transfer.model.json.JsonSupport
import tmaslanka.money.transfer.model.messages.{CreateAccountCommand, GetAccountQuery, MakeAccountTransactionCommand, TransactionDTO}
import tmaslanka.money.transfer.persistence.AccountRepository
import tmaslanka.money.transfer.service.AccountService

import scala.concurrent.ExecutionContext

class Server(settings: Settings,
             accountService: AccountService,
             accountRepository: AccountRepository)
            (implicit system: ActorSystem,
             mat: Materializer,
             ex: ExecutionContext) extends JsonSupport with StrictLogging {

  // @formatter:off
  val route: Route = logRequestResult(("request", Logging.InfoLevel)) {
      pathPrefix("v1") {
        accounts
      }
    }

  private def accounts: Route = pathPrefix("accounts") {
    pathEndOrSingleSlash {
      post {
        entity(as[CreateAccountCommand]) { request =>
          complete(accountService.createAccount(request))
        }
      } ~
      get {
        complete(accountService.findAccountNumbers)
      }
    } ~
    pathPrefix(Segment) { accountNo =>
      val accountNumber = AccountNumber(accountNo)
      onSuccess(accountRepository.findAccount(accountNumber)) {
        case None =>
          complete(StatusCodes.NotFound -> "")
        case Some(account) =>
          (pathEnd & get) {
            complete {
              accountService.getAccount(GetAccountQuery(accountNumber))
                .map(toResourceResponse)
            }
          } ~
          pathPrefix("transactions" ~ (Slash ~ PathEnd | PathEnd)) {
            get {
              complete(accountService.getTransactions(accountNumber))
            }
          } ~
          path("transactions" / Segment) { id =>
            val transactionId = TransactionId(id)
            get {
              complete {
                accountService.findTransaction(accountNumber, transactionId)
                  .map(toResourceResponse)
              }
            } ~
            put {
              entity(as[TransactionDTO]) { transaction =>
                complete(accountService.makeTransaction(account, MakeAccountTransactionCommand(transaction)).map(response =>
                if (response.error.isDefined) {
                  StatusCodes.BadRequest -> response
                } else {
                  StatusCodes.OK -> response
                }))
              }
            }
          }
      }
    }
  }
  // @formatter:on

  def start(): Unit = {
    logger.info(s"Starting server ${settings.host}:${settings.port}")
    Http().bindAndHandle(route, settings.host, settings.port)
  }

  private def toResourceResponse[A](maybeData: Option[A]): (StatusCode, Option[A]) = maybeData match {
    case Some(data) => StatusCodes.OK -> Some(data)
    case None => StatusCodes.NotFound -> None
  }
}
